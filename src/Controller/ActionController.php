<?php

namespace App\Controller;

use App\Entity\Action;
use App\Entity\Incident;
use App\Form\ActionFormType;
use App\Repository\ActionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class ActionController extends AbstractController
{
    /**
     * @Route("/action", name="action")
     */
    public function index(): Response
    {
        return $this->render('action/index.html.twig', [
            'controller_name' => 'ActionController',
        ]);
    }

    /**
     * Create action
     * @Route("/incident/{slug}/creer_un_evenement", name="add_action")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Incident $incident
     * @return Response
     */
    public function createAction(Request $request, EntityManagerInterface $em, Incident $incident): Response
    {
        $action = new Action();

        $form = $this->createForm(ActionFormType::class, $action);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $action
                ->setPilot($this->getUser())
                ->setIncident($incident);

            $em->persist($action);
            $em->flush();

            $this->addFlash(
                'success',
                'Evenement créé'
            );

            return $this->redirectToRoute('index_incident');
        }

        return $this->render('action/actionForm.html.twig', [
            'form' => $form->createView(),
            'operation' => 'Créer'
        ]);
    }

    /**
     * Updating an action
     * @Route("/incident/{slug}/modifier/{id}", name="update_action")
     * @param Action $action
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function updateAction(Action $action, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(ActionFormType::class, $action);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($action);
            $em->flush();

            $this->addFlash(
                'success',
                'Action mise à jour'
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        return $this->render('action/actionForm.html.twig', [
            'form' => $form->createView(),
            'operation' => 'Modifier'
        ]);
    }

    /**
     * Displays the details of an action
     * @Route("incident/{slug}/{id}", name="display_action")
     * @param ActionRepository $actionRepository
     * @param Action $action
     * @return Response
     */
    public function displayAction(ActionRepository $actionRepository, Action $action): Response
    {
        $action = $actionRepository->find($action);

        return $this->render('action/action.html.twig', [
            'action' => $action
        ]);
    }

    /**
     * Add participant
     * @Route("/incident/{slug}/{id}/add", name="add_user_an_action")
     * @param Action $action
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function addParticipant(Action $action, EntityManagerInterface $em): RedirectResponse
    {
        $action->addParticipant($this->getUser());
        $em->persist($action);
        $em->flush();

        return $this->redirectToRoute('display_action', [
            'slug' => $action->getIncident()->getSlug(),
            'id' => $action->getId(),
        ]);
    }

    /**
     * Add participant
     * @Route("/incident/{slug}/{id}/remove", name="remove_user_an_action")
     * @param Action $action
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function removeParticipant(Action $action, EntityManagerInterface $em): RedirectResponse
    {
        $action->removeParticipant($this->getUser());
        $em->persist($action);
        $em->flush();

        return $this->redirectToRoute('display_action', [
            'slug' => $action->getIncident()->getSlug(),
            'id' => $action->getId(),
        ]);
    }

    /**
     * Delete action
     * @Route("incident/{slug}/{id}/{_csrf_token}", name="delete_action")
     * @param Action $action
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param CsrfTokenManagerInterface $tokenManager
     * @return RedirectResponse
     */
    public function deleteAction(Action $action, Request $request, EntityManagerInterface $em, CsrfTokenManagerInterface $tokenManager): RedirectResponse
    {
        $token = new CsrfToken('delete_action', $request->attributes->get('_csrf_token'));

        if ($tokenManager->isTokenValid($token)) {
            $em->remove($action);
            $em->flush();

            $this->addFlash(
                'success',
                'Action supprimée'
            );

            return $this->redirectToRoute('display_incident', [
                'slug' => $action->getIncident()->getSlug(),
            ]);
        }

        $this->addFlash(
            'danger',
            'Token invalide. Veuillez réessayer.'
        );

        return $this->redirectToRoute('display_action', [
            'slug' => $action->getIncident()->getSlug(),
            'id' => $action->getId(),
        ]);
    }
}
