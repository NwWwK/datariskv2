<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserSearch;
use App\Form\PassFormType;
use App\Form\UpdateProfilFormType;
use App\Form\UserSearchFormType;
use App\Manager\User\UserManager;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class UserController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var UserManager
     */
    private UserManager $userManager;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param UserManager $userManager
     */
    public function __construct(UserRepository $userRepository, UserManager $userManager)
    {
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
    }

    /**
     * Edit profil user
     * @Route("/membre/{slug}/edition", name="app_update_user")
     * @Security("is_granted('ROLE_USER') and user.getSlug() === slug or is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour modifier ce profil !")
     * @param Request $request
     * @param User $user
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function updateUser(Request $request, User $user, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(UpdateProfilFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Le profil a été modifié.'
            );

            return $this->redirectToRoute('app_profil_user', ['slug' => $user->getSlug()]);
        }

        return $this->render('user/update_profil.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Update password
     * @Route("/membre/{slug}/creer-un-nouveau-mot-de-passe", name="app_update_password")
     * @Security("is_granted('ROLE_USER') and user.getSlug() === slug or is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour modifier ce profil !")
     * @param Request $request
     * @param User $user
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return mixed
     */
    public function updatePassword(
        Request $request,
        User $user,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $form = $this->createForm(PassFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Mot de passe mis à jour !');

            return $this->redirectToRoute('app_profil_user', ['slug' => $user->getSlug()]);
        }

        return $this->render('security/reset_password.html.twig',
            ['passForm' => $form->createView()]);
    }

    /**
     * List of users
     * @Route("/administration/users", name="index_users")
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour réaliser cette opération !")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param UserRepository $userRepository
     * @return Response
     */
    public function getListOfUsers(
        Request $request,
        PaginatorInterface $paginator,
        UserRepository $userRepository
    ): Response
    {
        $search = new UserSearch();
        $form = $this->createForm(UserSearchFormType::class, $search);
        $form->handleRequest($request);

        $users = $paginator->paginate(
            $data = $userRepository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
            ($search->getLimit() ? $search->getLimit() : 10)
        );


        return $this->render('user/index.html.twig', [
            'users' => $users,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Display profil user
     * @Route("/membre/{slug}", name="app_profil_user")
     * @param User $user
     * @param UserRepository $userRepository
     * @return Response
     */
    public function getProfilUser(User $user, UserRepository $userRepository): Response
    {
        $user = $userRepository->find($user);

        return $this->render('user/profil.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * Block user
     * @Route("/user/block/{slug}/{_csrf_token}", name="block_user")
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour réaliser cette opération !")
     * @param User $user
     * @param Request $request
     * @param CsrfTokenManagerInterface $csrfTokenManager
     * @return Response
     */
    public function banUser(User $user, Request $request, CsrfTokenManagerInterface $csrfTokenManager): Response
    {
        $token = new CsrfToken('block_user', $request->attributes->get('_csrf_token'));

        if ($csrfTokenManager->isTokenValid($token)) {
            $this->userManager->blockUser($user);

            return $this->redirectToRoute('index_users');
        }
        $this->addFlash('danger', 'Token invalide !'
        );

        return $this->redirectToRoute('index_users');
    }

    /**
     * Delete user
     * @Route("/user/delete/{slug}/{_csrf_token}", name="delete_user")
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour réaliser cette opération !")
     * @param User $user
     * @param Request $request
     * @param CsrfTokenManagerInterface $csrfTokenManager
     * @return Response
     */
    public function deleteUser(User $user, Request $request, CsrfTokenManagerInterface $csrfTokenManager): Response
    {
        $token = new CsrfToken('delete_user', $request->attributes->get('_csrf_token'));

        if ($csrfTokenManager->isTokenValid($token)) {
            $this->userManager->deleteUser($user);

            return $this->redirectToRoute('index_users');
        }

        $this->addFlash(
            'danger',
            'Token invalide !'
        );

        return $this->redirectToRoute('index_users');
    }
}
