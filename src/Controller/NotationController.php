<?php

namespace App\Controller;

use App\Entity\Notation;
use App\Form\NotationFormType;
use App\Repository\NotationRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class NotationController extends AbstractController
{
    /**
     * Get notations
     * @Route("/notation", name="index_notation")
     * @param NotationRepository $notationRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(NotationRepository $notationRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $notations = $paginator->paginate(
            $notationRepository->findBy([], ['id' => 'DESC']),
            $request->query->getInt('page', 1)
        );

        return $this->render('notation/index.html.twig', [
            'notations' => $notations,
        ]);
    }

    /**
     * Create notation
     * @Route("/notation/creer", name="create_notation")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @throws Exception
     */
    public function createNotation(Request $request, EntityManagerInterface $em): Response
    {
        $notation = new Notation();
        $form = $this->createForm(NotationFormType::class, $notation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $notation
                ->setDate(new DateTime(('now')))
                ->setCity($form->get('city')->getData())
                ->setAuthor($this->getUser());

            $em->persist($notation);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre note a été publié'
            );
            return $this->$this->redirectToRoute('index_notation');
        }
        return $this->render('notation/formNotation.html.twig', [
            'operation' => 'Créer',
            'form' => $form->createView()
        ]);
    }

    /**
     * Update notation
     * @param Notation $notation
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function updateNotation(Notation $notation, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(NotationFormType::class, $notation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $notation
                ->setCity($form->get('city')->getData())
                ->setNote($form->get('note')->getData())
                ->setAuthor($this->getUser());

            $em->persist($notation);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre note a été modifié !'
            );
            $this->redirectToRoute('index_notation');
        }
        return $this->render('notation/formNotation.html.twig', [
            'form' => $form->createView(),
            'operation' => 'Modifier'
        ]);
    }

    /**
     * Delete notation
     * @param Notation $notation
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param CsrfTokenManagerInterface $tokenManager
     * @return RedirectResponse
     */
    public function deleteNotation(Notation $notation, EntityManagerInterface $em, Request  $request, CsrfTokenManagerInterface $tokenManager)
    {
        $token = new CsrfToken('delete_notation', $request->attributes->get('_csrf_token'));

            if ($tokenManager->isTokenValid($token)) {
                $em->remove($notation);
                $em->flush();

                $this->addFlash(
                    'success',
                    'La note a été supprimée'
                );
                return $this->redirectToRoute('index_notation');
            }
            return $this->redirectToRoute('index_notation');
    }

    /**
     * Display notation
     * @param Notation $notation
     * @param NotationRepository $notationRepository
     * @return Response
     */
    public function getNotation(Notation $notation, NotationRepository $notationRepository)
    {
        $notation = $notationRepository->find($notation);

        return $this->render('notation/notation.html.twig', [
            'notation' => $notation
        ]);
    }
}
