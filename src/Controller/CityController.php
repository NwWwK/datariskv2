<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Incident;
use App\Repository\CityRepository;
use App\Repository\IncidentRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CityController extends AbstractController
{
    /**
     * @Route("/city", name="city")
     */
    public function index(): Response
    {
        return $this->render('city/index.html.twig', [
            'controller_name' => 'CityController',
        ]);
    }

    /**
     * @Route("/ville/{codeInsee}", name="show_city")
     * @param CityRepository $cityRepository
     * @param City $city
     * @return Response
     */
    public function showCity(CityRepository $cityRepository, City $city): Response
    {
        $city = $cityRepository->find($city);

        return $this->render('city/city.html.twig', [
            'city' => $city,
        ]);
    }

    /**
     * @Route("/ville/{codeInsee}/incidents", name="show_incident_city")
     * @param CityRepository $cityRepository
     * @param City $city
     * @param IncidentRepository $incidentRepository
     * @return Response
     */
    public function cityIncidents(CityRepository $cityRepository, City $city, IncidentRepository $incidentRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $data = $cityRepository->find($city);

        $incidents = $paginator->paginate(
            $data->getIncidents(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('incident/index.html.twig', [
            'incidents' => $incidents,
        ]);
    }
}
