<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\PassFormType;
use App\Form\ResetPassFormType;
use App\Repository\UserRepository;
use Exception;
use LogicException;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/deconnexion", name="app_logout")
     */
    public function logout()
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/oublie-mot-de-passe", name="app_forgotten_password")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param Swift_Mailer $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @return mixed
     */
    public function forgottenPass(
        Request $request,
        UserRepository $userRepository,
        Swift_Mailer $mailer,
        TokenGeneratorInterface $tokenGenerator
    ) {
        // Form initialization
        $form = $this->createForm(ResetPassFormType::class);

        // Processing the form
        $form->handleRequest($request);

        // If validated form
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            // Fetch data
            $user = $userRepository->findOneBy(['email' => $data['email']]);

            // Fetch user with email
            if (null === $user) {
                $this->addFlash('danger', 'Cette adresse n\'existe pas !');

                return $this->redirectToRoute('app_login');
            }

            //Token generator
            $token = $tokenGenerator->generateToken();

            // Save token in database
            try {
                $user->setResetToken($token);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
            } catch (Exception $e) {
                $this->addFlash('danger', $e->getMessage());
                return $this->redirectToRoute('home');
            }

            // Url generator
            $url = $this->generateUrl('app_reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);

            // Email creation
            $message = (new Swift_Message('Mot de passe oublié'))
                ->setFrom('stephane@portfolio.com')
                ->setTo($user->getEmail())
                ->setBody('Bonjour,<br><br>Une demande de réinitialisation de mot de passe à été effectuée. Pour créer votre nouveau mot de passe, veuillez cliquer sur le lien : <a href="' . $url . '">Créer un nouveau mot de passe</a>',
                    'text/html');

            // Send email
            $mailer->send($message);
            $this->addFlash('sucess', 'Email de réinitialisation envoyé !');

            return $this->redirectToRoute('home');
        }

        return $this->render('security/forgotten_password.html.twig',
            ['emailForm' => $form->createView()]);
    }

    /**
     * Reset password
     * @Route("/creer-mot-de-passe/{token}", name="app_reset_password")
     * @param Request $request
     * @param string $token
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return mixed
     */
    public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder)
    {
        // Form initialization
        $form = $this->createForm(PassFormType::class);

        // Processing the form
        $form->handleRequest($request);

        //Fetch user with token
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['resetToken' => $token]);

        // If unknown token
        if ($user == null) {
            $this->addFlash('danger', 'Token Inconnu');

            return $this->redirectToRoute('app_login');
        }

        // Processing form
        // If validated form
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setResetToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData()));

            // Save the data in database
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Mot de passe mis à jour !');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/reset_password.html.twig',
            ['passForm' => $form->createView()]);
        
    }


}
