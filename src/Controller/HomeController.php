<?php

namespace App\Controller;

use App\Entity\AirIndex;
use App\Entity\City;
use App\Entity\CitySearch;
use App\Entity\Coordinate;
use App\Entity\GeoLocation;
use App\Entity\Incident;
use App\Entity\SubIndice;
use App\Form\CitySearchFormType;
use App\Manager\Api\ApiManager;
use App\Repository\AirIndexRepository;
use App\Repository\CityRepository;
use App\Repository\CoordinateRepository;
use App\Repository\IncidentRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/search", name="city_search")
     * @param CityRepository $cityRepository
     * @param Request $request
     * @param AirIndexRepository $airIndexRepository
     * @param ApiManager $apiManager
     * @param EntityManagerInterface $em
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function searchCity(CityRepository $cityRepository, ApiManager $apiManager, EntityManagerInterface $em, Request $request, AirIndexRepository $airIndexRepository, SessionInterface $session): Response
    {
        $citySearch = new CitySearch();

        $form = $this->createForm(CitySearchFormType::class, $citySearch, [
            'action' => $this->generateUrl('city_search'),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->get('name')->getData();

            $city = $cityRepository->findOneBy([
                'codeInsee' => $data
            ]);

            $session = new Session();
            $session->start();

            $session->set('city', $city);

            $date = new DateTime('now');

            $airIndice = $airIndexRepository->findBy([
                'date' => $date->format('Y-m-d'),
                'city' => $city
            ]);

            if (!$airIndice) {
                $dataAir = $apiManager->getApiInformation(
                    'http://api.atmo-aura.fr/api/v1/communes/' . $city->getCodeInsee() . '/indices/atmo?api_token=3c2004d27fe8f8c100475720a6d7d6c2&page=2');

                foreach ($dataAir['data'] as $airIndice) {


                    if ($airIndice['date_echeance'] === $date->format('Y-m-d')) {

                        $newAirIndice = new AirIndex();

                        $newAirIndice
                            ->setDate($airIndice['date_echeance'])
                            ->setIndice($airIndice['indice'])
                            ->setQualifier($airIndice['qualificatif'])
                            ->setColor($airIndice['couleur_html'])
                            ->setType($airIndice['type_valeur'])
                            ->setCity($city);

                        $em->persist($newAirIndice);
                        $em->flush();

                        foreach ($airIndice['sous_indices'] as $sous_index) {
                            $newSubIndice = new SubIndice();

                            $newSubIndice
                                ->setPollutantName($sous_index['polluant_nom'])
                                ->setConcentration($sous_index['concentration'])
                                ->setSubIndiceIndex($sous_index['indice'])
                                ->setIndice($newAirIndice);

                            $em->persist($newSubIndice);
                            $em->flush();
                        }
                    }
                }
            }

            if (!$city->getGeoLocation()) {
                $apiResponse = $apiManager->getApiInformation('https://geo.api.gouv.fr/communes/' . $city->getCodeInsee() . '?fields=nom,code,codesPostaux,surface,contour,codeDepartement,codeRegion,population&format=json&geometry=centre');

                $newGeolocation = new GeoLocation();
                $newGeolocation->setType($apiResponse['contour']['type']);

                $city->setGeoLocation($newGeolocation);

                //Persist and flush GeoLocation
                $em->persist($newGeolocation);
                $em->flush();

                //Save gps coordinates
                foreach ($apiResponse['contour']['coordinates']['0'] as $detailedCity) {

                    //Checks if the data is of type float
                    if (is_float($detailedCity['0']) && is_float($detailedCity['1'])) {

                        //Adding data to the database
                        $newCoordinates = new Coordinate();
                        $newCoordinates
                            ->setGpsLon($detailedCity['0'])
                            ->setGpsLat($detailedCity['1'])
                            ->setGeoLocation($newGeolocation);

                        //Persist and flush coordinates
                        $em->persist($newCoordinates);
                        $em->flush();
                    }
                }

                //Persist and flush City
                $em->persist($city);
                $em->flush();
            }

            $datas = [];

            foreach ($city->getGeoLocation()->getCoordinates() as $key => $coordinate) {
                $datas[$key]['lat'] = $coordinate->getGpsLat();
                $datas[$key]['long'] = $coordinate->getGpsLon();
            }

            return $this->render('city/city.html.twig', [
                'city' => $city,
                'response' => $response = new JsonResponse($datas),
            ]);
        }

        return $this->render('general/city_search.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("cities/getAll", name="get_all_cities_name")
     * @param CityRepository $cityRepository
     * @return JsonResponse
     */
    function getAllCitiesNamesJson(CityRepository $cityRepository): JsonResponse
    {
        $data = $cityRepository->findAll();
        $cities = [];

        foreach ($data as $key => $detail) {
            $cities[$key]['name'] = $detail->getName();
            $cities[$key]['codePostal'] = $detail->getCodePostal();
            $cities[$key]['codeInsee'] = $detail->getCodeInsee();
        }

        return new JsonResponse($cities);
    }
}
