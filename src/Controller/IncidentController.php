<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Incident;
use App\Form\IncidentFormType;
use App\Repository\IncidentRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class IncidentController extends AbstractController
{
    /**
     * Get incidents
     * @Route("/incident", name="index_incident")
     * @param IncidentRepository $incidentRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(IncidentRepository $incidentRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $incidents = $paginator->paginate(
            $incidentRepository->findBy([], ['id' => 'DESC']),
            $request->query->getInt('page', 1)
        );

        return $this->render('incident/index.html.twig', [
            'incidents' => $incidents,
        ]);
    }

    /**
     * Create incident
     * @Route("/signalement/creer", name="create_incident")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function createIncident(Request $request, EntityManagerInterface $em): Response
    {
        $incident = new Incident();

        $form = $this->createForm(IncidentFormType::class, $incident);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $incident
                ->setDate(new DateTime(('now')))
                ->setCity($form->get('city')->getData())
                ->setCategory($form->get('category')->getData())
                ->setAuthor($this->getUser());

            $em->persist($incident);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre signalement a été publié'
            );

            return $this->redirectToRoute('index_incident');
        }

        return $this->render('incident/formIncident.html.twig', [
            'operation' => 'Créer',
            'form' => $form->createView()
        ]);
    }

    /**
     * Update incident
     * @Route("/incident/edition/{slug}", name="update_incident")
     * @Security("is_granted('ROLE_ADMIN') or user === incident.getUser()",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @param Incident $incident
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function updateIncident(Incident $incident, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(IncidentFormType::class, $incident);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $incident
                ->setCity($form->get('city')->getData())
                ->setCategory($form->get('category')->getData())
                ->setAuthor($this->getUser());

            $em->persist($incident);
            $em->flush();

            $this->addFlash(
                'success',
                'Signalement mis à jour !'
            );

            $this->redirectToRoute('index_incident');
        }

        return $this->render('incident/formIncident.html.twig', [
            'form' => $form->createView(),
            'operation' => 'Modifier'
        ]);
    }

    /**
     * Delete incident
     * @Route("/incident/suppression/{slug}/{_csrf_token}", name="delete_incident")
     * @Security("is_granted('ROLE_ADMIN') or user === incident.getUser()",
     *     message="Vous n'avez pas les droits pour supprimer cet article !")
     * @param Incident $incident
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param CsrfTokenManagerInterface $tokenManager
     * @return RedirectResponse
     */
    public function deleteIncident(Incident $incident, EntityManagerInterface $em, Request $request, CsrfTokenManagerInterface $tokenManager)
    {
        $token = new CsrfToken('delete_incident', $request->attributes->get('_csrf_token'));

        if ($tokenManager->isTokenValid($token)) {
            $em->remove($incident);
            $em->flush();

            $this->addFlash(
                'success',
                "Le signalement {$incident->getTitle()} a été supprimé"
            );

            return $this->redirectToRoute('index_incident');
        }

        return $this->redirectToRoute('index_incident');
    }

    /**
     * Display incident
     * @Route("incident/{slug}", name="display_incident")
     * @param Incident $incident
     * @param IncidentRepository $incidentRepository
     * @return Response
     */
    public function getIncident(Incident $incident, IncidentRepository $incidentRepository)
    {
        $incident = $incidentRepository->find($incident);

        return $this->render('incident/incident.html.twig', [
            'incident' => $incident
        ]);
    }
}
