<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryFormType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CategoryController extends AbstractController
{
    /**
     * @Route("/categories", name="index_categories")
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();

        return $this->render('category/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * Create a category
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/categorie/creer", name="create_category")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function createCategory(Request $request, EntityManagerInterface $em): Response
    {
        $category = new Category();

        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $category->setName(ucwords($category->getName()));

            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                'Catégorie ajoutée'
            );

            return $this->redirectToRoute('index_categories');
        }

        return $this->render('category/formCategory.html.twig', [
            'form' => $form->createView(),
            'operation' => 'Créer'
        ]);
    }

    /**
     * Update a category
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/categorie/modifier/{id}", name="update_category")
     * @param Request $request
     * @param Category $category
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function updateCategory(Request $request, Category $category, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $category->setName(ucwords($category->getName()));

            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                'Categorie modifiée'
            );

            return $this->redirectToRoute('index_categories');
        }

        return $this->render('category/formCategory.html.twig', [
            'operation' => 'Modifier',
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * Delete category
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/categorie/suppression/{id}/{_csrf_token}", name="delete_category")
     * @param Category $category
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param CsrfTokenManagerInterface $tokenManager
     * @return RedirectResponse
     */
    public function deleteCategory(Category $category, EntityManagerInterface $em, Request $request, CsrfTokenManagerInterface $tokenManager): RedirectResponse
    {
        $token = new CsrfToken('delete_category', $request->attributes->get('_csrf_token'));

        if ($tokenManager->isTokenValid($token)) {
            $em->remove($category);
            $em->flush();

            $this->addFlash(
                'success',
                'Catégorie supprimé'
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $this->addFlash(
            'danger',
            'Token invalid ! Veuillez réessayer.'
        );

        return $this->redirectToRoute('index_categories');
    }
}
