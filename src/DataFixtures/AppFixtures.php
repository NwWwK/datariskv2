<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        $user = new User();
        $password = $this->encoder->encodePassword($user, 'password');
        $user
            ->setFirstName('Admin')
            ->setLastName('Admin')
            ->setEmail('Admin@gmail.com')
            ->setPassword($password)
            ->setIsVerified(1)
            ->setIsEnabled(1)
            ->setRegistrationDate(new DateTime($faker->date('Y-m-d')))
            ->setRoles(["ROLE_ADMIN"]);

        $manager->persist($user);
        $users[] = $user;


        for ($i = 1; $i <= 25; $i++) {
            $user = new User();

            $password = $this->encoder->encodePassword($user, 'password');

            $user
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setPassword($password)
                ->setIsVerified(mt_rand(0, 1))
                ->setIsEnabled(mt_rand(0, 1))
                ->setRegistrationDate(new DateTime($faker->date('Y-m-d')))
                ->setRoles(["ROLE_USER"]);

            $manager->persist($user);
            $users[] = $user;
        }

            $manager->flush($users);
    }
}
