<?php

namespace App\Entity;

use App\Repository\RiskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RiskRepository::class)
 */
class Risk
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=FamilyRisk::class, inversedBy="risk")
     */
    private $familyRisk;

    /**
     * @ORM\OneToMany(targetEntity=RiskLevel::class, mappedBy="Risk")
     */
    private $riskLevels;

    public function __construct()
    {
        $this->city = new ArrayCollection();
        $this->riskLevels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFamilyRisk(): ?FamilyRisk
    {
        return $this->familyRisk;
    }

    public function setFamilyRisk(?FamilyRisk $familyRisk): self
    {
        $this->familyRisk = $familyRisk;

        return $this;
    }

    /**
     * @return Collection|RiskLevel[]
     */
    public function getRiskLevels(): Collection
    {
        return $this->riskLevels;
    }

    public function addRiskLevel(RiskLevel $riskLevel): self
    {
        if (!$this->riskLevels->contains($riskLevel)) {
            $this->riskLevels[] = $riskLevel;
            $riskLevel->setRisk($this);
        }

        return $this;
    }

    public function removeRiskLevel(RiskLevel $riskLevel): self
    {
        if ($this->riskLevels->removeElement($riskLevel)) {
            // set the owning side to null (unless already changed)
            if ($riskLevel->getRisk() === $this) {
                $riskLevel->setRisk(null);
            }
        }

        return $this;
    }
}
