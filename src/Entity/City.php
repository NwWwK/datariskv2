<?php

namespace App\Entity;

use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CityRepository::class)
 */
class City
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $codeInsee;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $populate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $surface;

    /**
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="cities")
     */
    private $department;

    /**
     * @ORM\OneToOne(targetEntity=GeoLocation::class, cascade={"persist", "remove"})
     */
    private $geoLocation;

    /**
     * @ORM\OneToMany(targetEntity=Seveso::class, mappedBy="city")
     */
    private $sevesos;

    /**
     * @ORM\OneToMany(targetEntity=NaturalDisaster::class, mappedBy="city")
     */
    private $naturalDisasters;

    /**
     * @ORM\OneToMany(targetEntity=Incident::class, mappedBy="city")
     */
    private $incidents;

    /**
     * @ORM\OneToMany(targetEntity=AirIndex::class, mappedBy="city")
     */
    private $airIndices;

    /**
     * @ORM\OneToMany(targetEntity=Notation::class, mappedBy="city", orphanRemoval=true)
     */
    private $notations;

    public function __construct()
    {
        $this->sevesos = new ArrayCollection();
        $this->naturalDisasters = new ArrayCollection();
        $this->risks = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->airIndices = new ArrayCollection();
        $this->notations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeInsee(): ?string
    {
        return $this->codeInsee;
    }

    public function setCodeInsee(string $codeInsee): self
    {
        $this->codeInsee = $codeInsee;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getPopulate(): ?int
    {
        return $this->populate;
    }

    public function setPopulate(?int $populate): self
    {
        $this->populate = $populate;

        return $this;
    }

    public function getSurface(): ?float
    {
        return $this->surface;
    }

    public function setSurface(?float $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getGeoLocation(): ?GeoLocation
    {
        return $this->geoLocation;
    }

    public function setGeoLocation(?GeoLocation $geoLocation): self
    {
        $this->geoLocation = $geoLocation;

        return $this;
    }

    /**
     * @return Collection|Seveso[]
     */
    public function getSevesos(): Collection
    {
        return $this->sevesos;
    }

    public function addSeveso(Seveso $seveso): self
    {
        if (!$this->sevesos->contains($seveso)) {
            $this->sevesos[] = $seveso;
            $seveso->setCity($this);
        }

        return $this;
    }

    public function removeSeveso(Seveso $seveso): self
    {
        if ($this->sevesos->removeElement($seveso)) {
            // set the owning side to null (unless already changed)
            if ($seveso->getCity() === $this) {
                $seveso->setCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NaturalDisaster[]
     */
    public function getNaturalDisasters(): Collection
    {
        return $this->naturalDisasters;
    }

    public function addNaturalDisaster(NaturalDisaster $naturalDisaster): self
    {
        if (!$this->naturalDisasters->contains($naturalDisaster)) {
            $this->naturalDisasters[] = $naturalDisaster;
            $naturalDisaster->setCity($this);
        }

        return $this;
    }

    public function removeNaturalDisaster(NaturalDisaster $naturalDisaster): self
    {
        if ($this->naturalDisasters->removeElement($naturalDisaster)) {
            // set the owning side to null (unless already changed)
            if ($naturalDisaster->getCity() === $this) {
                $naturalDisaster->setCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setCity($this);
        }

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->removeElement($incident)) {
            // set the owning side to null (unless already changed)
            if ($incident->getCity() === $this) {
                $incident->setCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AirIndex[]
     */
    public function getAirIndices(): Collection
    {
        return $this->airIndices;
    }

    public function addAirIndex(AirIndex $airIndex): self
    {
        if (!$this->airIndices->contains($airIndex)) {
            $this->airIndices[] = $airIndex;
            $airIndex->setCity($this);
        }

        return $this;
    }

    public function removeAirIndex(AirIndex $airIndex): self
    {
        if ($this->airIndices->removeElement($airIndex)) {
            // set the owning side to null (unless already changed)
            if ($airIndex->getCity() === $this) {
                $airIndex->setCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notation[]
     */
    public function getNotations(): Collection
    {
        return $this->notations;
    }

    public function addNotation(Notation $notation): self
    {
        if (!$this->notations->contains($notation)) {
            $this->notations[] = $notation;
            $notation->setCity($this);
        }

        return $this;
    }

    public function removeNotation(Notation $notation): self
    {
        if ($this->notations->removeElement($notation)) {
            // set the owning side to null (unless already changed)
            if ($notation->getCity() === $this) {
                $notation->setCity(null);
            }
        }

        return $this;
    }

}
