<?php

namespace App\Entity;

use App\Repository\AirIndexRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AirIndexRepository::class)
 */
class AirIndex
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $indice;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $qualifier;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="airIndices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity=SubIndice::class, mappedBy="indice")
     */
    private $subIndices;

    public function __construct()
    {
        $this->subIndices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIndice(): ?int
    {
        return $this->indice;
    }

    public function setIndice(int $indice): self
    {
        $this->indice = $indice;

        return $this;
    }

    public function getQualifier(): ?string
    {
        return $this->qualifier;
    }

    public function setQualifier(string $qualifier): self
    {
        $this->qualifier = $qualifier;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|SubIndice[]
     */
    public function getSubIndices(): Collection
    {
        return $this->subIndices;
    }

    public function addSubIndex(SubIndice $subIndex): self
    {
        if (!$this->subIndices->contains($subIndex)) {
            $this->subIndices[] = $subIndex;
            $subIndex->setIndice($this);
        }

        return $this;
    }

    public function removeSubIndex(SubIndice $subIndex): self
    {
        if ($this->subIndices->removeElement($subIndex)) {
            // set the owning side to null (unless already changed)
            if ($subIndex->getIndice() === $this) {
                $subIndex->setIndice(null);
            }
        }

        return $this;
    }
}
