<?php

namespace App\Entity;

use App\Repository\SevesoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SevesoRepository::class)
 */
class Seveso
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $codeInsee;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $family;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sevesoThreshold;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $iccp;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="sevesos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\OneToOne(targetEntity=GeoLocation::class, cascade={"persist", "remove"})
     */
    private $geoLocation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getCodeInsee(): ?string
    {
        return $this->codeInsee;
    }

    public function setCodeInsee(string $codeInsee): self
    {
        $this->codeInsee = $codeInsee;

        return $this;
    }

    public function getFamily(): ?string
    {
        return $this->family;
    }

    public function setFamily(string $family): self
    {
        $this->family = $family;

        return $this;
    }

    public function getSevesoThreshold(): ?string
    {
        return $this->sevesoThreshold;
    }

    public function setSevesoThreshold(string $sevesoThreshold): self
    {
        $this->sevesoThreshold = $sevesoThreshold;

        return $this;
    }

    public function getIccp(): ?string
    {
        return $this->iccp;
    }

    public function setIccp(string $iccp): self
    {
        $this->iccp = $iccp;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getGeoLocation(): ?GeoLocation
    {
        return $this->geoLocation;
    }

    public function setGeoLocation(?GeoLocation $geoLocation): self
    {
        $this->geoLocation = $geoLocation;

        return $this;
    }
}
