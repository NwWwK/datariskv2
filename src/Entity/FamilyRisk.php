<?php

namespace App\Entity;

use App\Repository\FamilyRiskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FamilyRiskRepository::class)
 */
class FamilyRisk
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Risk::class, mappedBy="familyRisk")
     */
    private $risk;

    public function __construct()
    {
        $this->risk = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Risk[]
     */
    public function getRisk(): Collection
    {
        return $this->risk;
    }

    public function addRisk(Risk $risk): self
    {
        if (!$this->risk->contains($risk)) {
            $this->risk[] = $risk;
            $risk->setFamilyRisk($this);
        }

        return $this;
    }

    public function removeRisk(Risk $risk): self
    {
        if ($this->risk->removeElement($risk)) {
            // set the owning side to null (unless already changed)
            if ($risk->getFamilyRisk() === $this) {
                $risk->setFamilyRisk(null);
            }
        }

        return $this;
    }
}
