<?php

namespace App\Entity;

use App\Repository\GeoLocationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GeoLocationRepository::class)
 */
class GeoLocation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Coordinate::class, fetch="EAGER", mappedBy="geoLocation")
     */
    private $coordinates;

    public function __construct()
    {
        $this->coordinates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Coordinate[]
     */
    public function getCoordinates(): Collection
    {
        return $this->coordinates;
    }

    public function addCoordinate(Coordinate $coordinate): self
    {
        if (!$this->coordinates->contains($coordinate)) {
            $this->coordinates[] = $coordinate;
            $coordinate->setGeoLocation($this);
        }

        return $this;
    }

    public function removeCoordinate(Coordinate $coordinate): self
    {
        if ($this->coordinates->removeElement($coordinate)) {
            // set the owning side to null (unless already changed)
            if ($coordinate->getGeoLocation() === $this) {
                $coordinate->setGeoLocation(null);
            }
        }

        return $this;
    }
}
