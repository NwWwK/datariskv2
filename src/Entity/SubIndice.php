<?php

namespace App\Entity;

use App\Repository\SubIndiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubIndiceRepository::class)
 */
class SubIndice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $PollutantName;

    /**
     * @ORM\Column(type="integer")
     */
    private $concentration;

    /**
     * @ORM\ManyToOne(targetEntity=AirIndex::class, inversedBy="subIndices")
     */
    private $indice;

    /**
     * @ORM\Column(type="integer")
     */
    private $SubIndiceIndex;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPollutantName(): ?string
    {
        return $this->PollutantName;
    }

    public function setPollutantName(string $PollutantName): self
    {
        $this->PollutantName = $PollutantName;

        return $this;
    }

    public function getConcentration(): ?int
    {
        return $this->concentration;
    }

    public function setConcentration(int $concentration): self
    {
        $this->concentration = $concentration;

        return $this;
    }

    public function getIndice(): ?AirIndex
    {
        return $this->indice;
    }

    public function setIndice(?AirIndex $indice): self
    {
        $this->indice = $indice;

        return $this;
    }

    public function getSubIndiceIndex(): ?int
    {
        return $this->SubIndiceIndex;
    }

    public function setSubIndiceIndex(int $SubIndiceIndex): self
    {
        $this->SubIndiceIndex = $SubIndiceIndex;

        return $this;
    }
}
