<?php

namespace App\Entity;

use App\Repository\RiskLevelRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RiskLevelRepository::class)
 */
class RiskLevel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Level;

    /**
     * @ORM\ManyToOne(targetEntity=Risk::class, inversedBy="riskLevels")
     */
    private $Risk;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLevel(): ?string
    {
        return $this->Level;
    }

    public function setLevel(string $Level): self
    {
        $this->Level = $Level;

        return $this;
    }

    public function getRisk(): ?Risk
    {
        return $this->Risk;
    }

    public function setRisk(?Risk $Risk): self
    {
        $this->Risk = $Risk;

        return $this;
    }
}
