<?php

namespace App\Entity;

use App\Repository\NaturalDisasterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NaturalDisasterRepository::class)
 */
class NaturalDisaster
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeNatCatna;

    /**
     * @ORM\Column(type="date")
     */
    private $eventStartDate;

    /**
     * @ORM\Column(type="date")
     */
    private $eventEndDate;

    /**
     * @ORM\Column(type="date")
     */
    private $publicationOrderDate;

    /**
     * @ORM\Column(type="date")
     */
    private $publicationJoDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wordingRiskJo;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="naturalDisasters")
     */
    private $city;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeNatCatna(): ?string
    {
        return $this->codeNatCatna;
    }

    public function setCodeNatCatna(string $codeNatCatna): self
    {
        $this->codeNatCatna = $codeNatCatna;

        return $this;
    }

    public function getEventStartDate(): ?\DateTimeInterface
    {
        return $this->eventStartDate;
    }

    public function setEventStartDate(\DateTimeInterface $eventStartDate): self
    {
        $this->eventStartDate = $eventStartDate;

        return $this;
    }

    public function getEventEndDate(): ?\DateTimeInterface
    {
        return $this->eventEndDate;
    }

    public function setEventEndDate(\DateTimeInterface $eventEndDate): self
    {
        $this->eventEndDate = $eventEndDate;

        return $this;
    }

    public function getPublicationOrderDate(): ?\DateTimeInterface
    {
        return $this->publicationOrderDate;
    }

    public function setPublicationOrderDate(\DateTimeInterface $publicationOrderDate): self
    {
        $this->publicationOrderDate = $publicationOrderDate;

        return $this;
    }

    public function getPublicationJoDate(): ?\DateTimeInterface
    {
        return $this->publicationJoDate;
    }

    public function setPublicationJoDate(\DateTimeInterface $publicationJoDate): self
    {
        $this->publicationJoDate = $publicationJoDate;

        return $this;
    }

    public function getWordingRiskJo(): ?string
    {
        return $this->wordingRiskJo;
    }

    public function setWordingRiskJo(string $wordingRiskJo): self
    {
        $this->wordingRiskJo = $wordingRiskJo;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }
}
