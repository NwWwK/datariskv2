<?php

namespace App\Entity;

use App\Repository\CityRiskRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CityRiskRepository::class)
 */
class CityRisk
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="cityRisks")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?City $City;

    /**
     * @ORM\ManyToOne(targetEntity=Risk::class, inversedBy="cityRisks")
     */
    private ?Risk $Risk;

    /**
     * @ORM\ManyToOne(targetEntity=RiskLevel::class, inversedBy="cityRisks")
     */
    private ?RiskLevel $RiskLevel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?City
    {
        return $this->City;
    }

    public function setCity(?City $City): self
    {
        $this->City = $City;

        return $this;
    }

    public function getRisk(): ?Risk
    {
        return $this->Risk;
    }

    public function setRisk(?Risk $Risk): self
    {
        $this->Risk = $Risk;

        return $this;
    }

    public function getRiskLevel(): ?RiskLevel
    {
        return $this->RiskLevel;
    }

    public function setRiskLevel(?RiskLevel $RiskLevel): self
    {
        $this->RiskLevel = $RiskLevel;

        return $this;
    }
}
