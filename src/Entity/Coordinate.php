<?php

namespace App\Entity;

use App\Repository\CoordinateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoordinateRepository::class)
 */
class Coordinate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $gpsLat;

    /**
     * @ORM\Column(type="float")
     */
    private $gpsLon;

    /**
     * @ORM\ManyToOne(targetEntity=GeoLocation::class, inversedBy="coordinates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $geoLocation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGpsLat(): ?float
    {
        return $this->gpsLat;
    }

    public function setGpsLat(float $gpsLat): self
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    public function getGpsLon(): ?float
    {
        return $this->gpsLon;
    }

    public function setGpsLon(float $gpsLon): self
    {
        $this->gpsLon = $gpsLon;

        return $this;
    }

    public function getGeoLocation(): ?GeoLocation
    {
        return $this->geoLocation;
    }

    public function setGeoLocation(?GeoLocation $geoLocation): self
    {
        $this->geoLocation = $geoLocation;

        return $this;
    }
}
