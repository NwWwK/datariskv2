<?php

namespace App\Entity;

/**
 * Class CitySearch
 * @package App\Entity
 */
class CitySearch {

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var string|null
     */
    private ?string $codePostal = null;

    /**
     * @var string|null
     */
    private ?string $codeInsee = null;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    /**
     * @param string|null $codePostal
     */
    public function setCodePostal(?string $codePostal): void
    {
        $this->codePostal = $codePostal;
    }

    /**
     * @return string|null
     */
    public function getCodeInsee(): ?string
    {
        return $this->codeInsee;
    }

    /**
     * @param string|null $codeInsee
     */
    public function setCodeInsee(?string $codeInsee): void
    {
        $this->codeInsee = $codeInsee;
    }

}