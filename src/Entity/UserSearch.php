<?php

namespace App\Entity;

class UserSearch
{
    /**
     * @var string|null
     */
    private ?string $firstName = null;

    /**
     * @var string|null
     */
    private ?string $lastName = null;

    /**
     * @var null
     */
    private $registrationStart = null;

    /**
     * @var null
     */
    private $registrationEnd = null;

    /**
     * @var string|null
     */
    private ?string $email = null;

    /**
     * @var bool
     */
    private ?bool $isVerified = null;

    /**
     * @var bool
     */
    private ?bool $isEnabled = null;

    /**
     * @var string|null
     */
    private ?string $roles = null;

    /**
     * @var int|null
     */
    private ?int $limit = null;

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return bool
     */
    public function isVerified(): ?bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     * @return UserSearch
     */
    public function setIsVerified(bool $isVerified): UserSearch
    {
        $this->isVerified = $isVerified;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     */
    public function setIsEnabled(bool $isEnabled): void
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return string|null
     */
    public function getRoles(): ?string
    {
        return $this->roles;
    }

    /**
     * @param string $roles
     */
    public function setRoles(string $roles): void
    {
        $this->roles = $roles;
    }
    
    /**
     * @return null
     */
    public function getRegistrationStart()
    {
        return $this->registrationStart;
    }

    /**
     * @param null $registrationStart
     */
    public function setRegistrationStart($registrationStart): void
    {
        $this->registrationStart = $registrationStart;
    }

    /**
     * @return null
     */
    public function getRegistrationEnd()
    {
        return $this->registrationEnd;
    }

    /**
     * @param null $registrationEnd
     */
    public function setRegistrationEnd($registrationEnd): void
    {
        $this->registrationEnd = $registrationEnd;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }


}