<?php

namespace App\Command;

use App\Entity\CityRisk;
use App\Entity\FamilyRisk;
use App\Entity\Risk;
use App\Entity\RiskLevel;
use App\Manager\Api\ApiManager;
use App\Repository\CityRepository;
use App\Repository\CityRiskRepository;
use App\Repository\FamilyRiskRepository;
use App\Repository\RiskLevelRepository;
use App\Repository\RiskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RiskCommand extends Command
{
    protected static $defaultName = 'app:risk:all';

    /**
     * @var RiskRepository $riskRepository
     */
    private RiskRepository $riskRepository;

    /**
     * @var FamilyRiskRepository $familyRiskRepository
     */
    private FamilyRiskRepository $familyRiskRepository;

    /**
     * @var RiskLevelRepository $riskLevelRepository
     */
    private RiskLevelRepository $riskLevelRepository;

    /**
     * @var CityRiskRepository $cityRiskRepository
     */
    private CityRiskRepository $cityRiskRepository;

    /**
     * @var CityRepository $cityRepository
     */
    private CityRepository $cityRepository;

    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * @var ApiManager $apiManager
     */
    private ApiManager $apiManager;

    /**
     * RiskCommand constructor.
     * @param RiskRepository $riskRepository
     * @param FamilyRiskRepository $familyRiskRepository
     * @param RiskLevelRepository $riskLevelRepository
     * @param CityRiskRepository $cityRiskRepository
     * @param CityRepository $cityRepository
     * @param EntityManagerInterface $em
     * @param ApiManager $apiManager
     */
    public function __construct(
        RiskRepository $riskRepository,
        FamilyRiskRepository $familyRiskRepository,
        RiskLevelRepository $riskLevelRepository,
        CityRiskRepository $cityRiskRepository,
        CityRepository $cityRepository,
        EntityManagerInterface $em,
        ApiManager $apiManager
    )
    {
        $this->riskRepository = $riskRepository;
        $this->familyRiskRepository = $familyRiskRepository;
        $this->riskLevelRepository = $riskLevelRepository;
        $this->cityRiskRepository = $cityRiskRepository;
        $this->cityRepository = $cityRepository;
        $this->em = $em;
        $this->apiManager = $apiManager;
        parent::__construct();
    }

    /**
     * RiskCommand Configure
     */
    public function configure()
    {
        $this
            ->setName('app:risk:all')
            ->setDescription('Execute app:risk:all to fetch all risks and family risk');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        //Get family risk data on georisques.gouv.fr
        $dataRisk = $this->apiManager->getApiInformation('https://www.georisques.gouv.fr/api/v1/ppr/famille_risques');

        //Launch progressbar
        $progressBar = new ProgressBar($output, count($dataRisk));
        $progressBar->start();

        //Save in database
        foreach ($dataRisk['data'] as $familyRisk) {

            //Check if family risk exist
            $existFamilyRisk = $this->familyRiskRepository->findOneBy(['name' => $familyRisk['libelle_risque']]);

            //If NOT exist
            if (empty($existFamilyRisk)) {

                //Create new Family Risk
                $newFamilyRisk = new FamilyRisk();
                $newFamilyRisk
                    ->setCode($familyRisk['code_risque'])
                    ->setName($familyRisk['libelle_risque']);

                //Persist and flush Family Risk
                $this->em->persist($newFamilyRisk);
                $this->em->flush();
            }

            foreach ($familyRisk['classes_alea'] as $risk) {

                //Check if risk exist
                $existRisk = $this->riskRepository->findOneBy(['name' => $risk['libelle']]);

                //If NOT exist
                if (empty($existRisk)) {
                    $newRisk = new Risk();
                    $newRisk
                        ->setCode($risk['code'])
                        ->setName($risk['libelle'])
                        ->setFamilyRisk($newFamilyRisk);

                    //Persist and flush risk
                    $this->em->persist($newRisk);
                    $this->em->flush();
                }
            }
            //Advance progressbar
            $progressBar->advance();
        }
        //Finish progressbar
        $progressBar->finish();

        //Collect the RISKS of each city present in the database
        $codesInsee = $this->cityRepository->getCodeInseeCities();

        //Launch progressbar
        $progressBar = new ProgressBar($output, count($codesInsee));
        $progressBar->start();

        //Research the risks of cities via the online API Georisques.com
        foreach ($codesInsee as $codeInsee) {

            $cityRisks = $this->apiManager->getApiInformation('https://www.georisques.gouv.fr/api/v1/gaspar/risques?rayon=1000&code_insee=' . $codeInsee['codeInsee'] . '&page=1&page_size=100');

            // If cityRisks is not empty
            if ($cityRisks['data'] != null) {

                foreach ($cityRisks['data']['0']['risques_detail'] as $risk) {

                    // Retrieves the risk and the city from the database
                    $risk = $this->riskRepository->findOneBy(['name' => $risk['libelle_risque_long']]);
                    $city = $this->cityRepository->findOneBy(['codeInsee' => $codeInsee['codeInsee']]);

                    //Check if not exist && if radon risk level is not null
                    $cityRisk = $this->cityRiskRepository->findOneBy(['City' => $city, 'Risk' => $risk]);

                    //If cityRisk is not exist and risk is not null
                    if (!$cityRisk && $risk) {

                        $newCityRisk = new CityRisk();
                        $newCityRisk
                            ->setRisk($risk)
                            ->setCity($city);

                        //Recover the earthquake risk level
                        if ($risk->getName() === 'Séisme') {

                            // Collect level earthquake in API Georisques and check if exist in database
                            $levelSeisme = $this->apiManager->getApiInformation('https://www.georisques.gouv.fr/api/v1/zonage_sismique?rayon=1000&code_insee=' . $codeInsee['codeInsee'] . '&page=1&page_size=10');
                            $existLvlSeisme = $this->riskLevelRepository->findOneBy(['Level' => $levelSeisme['data']['0']['zone_sismicite']]);

                            //If not exist earthquake level in database
                            if (empty($existLvlSeisme)) {

                                $newLvlSeisme = new RiskLevel();
                                $newLvlSeisme
                                    ->setLevel($levelSeisme['data']['0']['zone_sismicite'])
                                    ->setRisk($risk);

                                //Persist and flush new Level Seisme
                                $this->em->persist($newLvlSeisme);
                                $this->em->flush();

                                $existLvlSeisme = $newLvlSeisme;
                            }
                            //Add Risk Level in CityRisk
                            $newCityRisk->setRiskLevel($existLvlSeisme);
                        }
                        //Persist and flush add Risk for city
                        $this->em->persist($newCityRisk);
                        $this->em->flush();
                    }
                }
            }
            //Recover the radon risk level
            $levelRadon = $this->apiManager->getApiInformation('https://www.georisques.gouv.fr/api/v1/radon?rayon=1000&code_insee=' . $codeInsee['codeInsee'] . '&page=1&page_size=10');

            if (!empty($levelRadon['data']['0']['classe_potentiel'])) {

                // Retrieves level and risk in the database
                $existLvlRadon = $this->riskLevelRepository->findOneBy(['Level' => $levelRadon['data']['0']['classe_potentiel']]);
                $risk = $this->riskRepository->findOneBy(['name' => 'Radon']);

                //Check if not exist && if radon risk level is not null
                $cityRisk = $this->cityRiskRepository->findOneBy(['City' => $city, 'Risk' => $risk]);

                if (!$cityRisk) {
                    //If no radon level is present in the database, save the level
                    if (empty($existLvlRadon)) {

                        $newLvlRadon = new RiskLevel();
                        $newLvlRadon
                            ->setLevel($levelRadon['data']['0']['classe_potentiel'])
                            ->setRisk($risk);

                        //Persist and flush new level radon
                        $this->em->persist($newLvlRadon);
                        $this->em->flush();

                        $existLvlRadon = $newLvlRadon;
                    }

                    //Add Level Radon in City Risk
                    if ($risk) {

                        $newCityRisk = new CityRisk();
                        $newCityRisk
                            ->setRisk($risk)
                            ->setCity($city)
                            ->setRiskLevel($existLvlRadon);

                        //Persist and flush new City Risk Radon
                        $this->em->persist($newCityRisk);
                        $this->em->flush();
                    }
                }

            }
            //Advance progressbar
            $progressBar->advance();
        }
        //Finish progressbar
        $progressBar->finish();

        //End of command
        return command::SUCCESS;
    }
}
