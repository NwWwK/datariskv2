<?php

namespace App\Command;

use App\Entity\Coordinate;
use App\Entity\GeoLocation;
use App\Entity\Seveso;
use App\Manager\Api\ApiManager;
use App\Repository\CityRepository;
use App\Repository\SevesoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SevesoCommand extends Command
{
    protected static $defaultName = 'app:seveso:all';
    /**
     * @var SevesoRepository $sevesoRespository
     */
    private SevesoRepository $sevesoRespository;
    /**
     * @var CityRepository $cityRepository
     */
    private CityRepository $cityRepository;
    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;
    /**
     * @var ApiManager $apiManager
     */
    private ApiManager $apiManager;

    /**
     * SevesoCommand constructor.
     * @param SevesoRepository $sevesoRepository
     * @param CityRepository $cityRepository
     * @param EntityManagerInterface $em
     * @param ApiManager $apiManager
     */
    public function __construct(SevesoRepository $sevesoRepository, CityRepository $cityRepository, EntityManagerInterface $em, ApiManager $apiManager)
    {
        $this->sevesoRespository = $sevesoRepository;
        $this->cityRepository = $cityRepository;
        $this->em = $em;
        $this->apiManager = $apiManager;
        parent::__construct();
    }

    /**
     * SevesoCommand configuration
     */
    protected function configure()
    {
        $this
            ->setName('app:seveso:all')
            ->setDescription('Execute app:seveso to fetch all Seveso on API');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Get list of SEVESOS
        $nbRows = 2000;
        $sevesosList = $this->apiManager->getApiInformation('https://public.opendatasoft.com/api/records/1.0/search/?dataset=sites-seveso&q=&rows=' . $nbRows);

        //Launch progress bar
        $progressBar = new ProgressBar($output, count($sevesosList));
        $progressBar->start();

        //Save Seveso
        foreach ($sevesosList['records'] as $seveso) {

            if ($seveso['c_num_dep'] === '63') {


                //Check if exist
                $existSeveso = $this->sevesoRespository->findOneBy(['name' => $seveso['fields']['c_nom_ets']]);

                //If NOT exist
                if (empty($existSeveso)) {
                    // Create new Geolocation
                    $newGeoLocation = new GeoLocation();
                    $newGeoLocation->setType($seveso['geometry']['type']);

                    //Persist and flush Geolocation
                    $this->em->persist($newGeoLocation);
                    $this->em->flush();

                    //Create new Seveso
                    $newSeveso = new Seveso();

                    $newSeveso
                        ->setName($seveso['fields']['c_nom_ets'])
                        ->setCodePostal($seveso['fields']['c_cp'])
                        ->setFamily($seveso['fields']['c_famill_l'])
                        ->setSevesoThreshold($seveso['fields']['c_seveso_l'])
                        ->setIccp($seveso['fields']['c_ippc_lib'])
                        ->setGeoLocation($newGeoLocation);

                    //If data missing
                    (empty($seveso['fields']['c_num_sire'])) ? $newSeveso->setSiret('Non indiqué') :
                        $newSeveso->setSiret($seveso['fields']['c_num_sire']);
                    (empty($seveso['fields']['nom_epci'])) ? $newSeveso->setAdress('Non indiquée') :
                        $newSeveso->setAdress($seveso['fields']['nom_epci']);

                    //If insee code absent, search for informations by city name in BDD Cities + SetCity && setCodeInsee
                    if (!empty($seveso['fields']['code_insee'])) {

                        //Get city by code insee
                        $city = $this->cityRepository->findOneBy(['codeInsee' => $seveso['fields']['code_insee']]);

                        //SetCity && SetCodeInsee
                        $newSeveso
                            ->setCity($city)
                            ->setCodeInsee($seveso['fields']['code_insee']);

                    } else {
                        // Else, fetch city
                        $responseCity = $this->cityRepository->findOneBy(['name' => mb_strtolower($seveso['fields']['c_nom_com'])]);
                        $codeInsee = $responseCity->getCodeinsee();

                        $newSeveso
                            ->setCodeInsee($codeInsee)
                            ->setCity($responseCity);
                    }

                    //Save SEVESO in database
                    $this->em->persist($newSeveso);
                    $this->em->flush();
                }

                //Add GPS location
                if (is_float($seveso['fields']['geo_point_2d']['0']) && is_float($seveso['fields']['geo_point_2d']['1'])) {

                    $newCoordinates = new Coordinate();
                    $newCoordinates
                        ->setGeoLocation($newGeoLocation)
                        ->setGpsLat($seveso['fields']['geo_point_2d']['0'])
                        ->setGpsLon($seveso['fields']['geo_point_2d']['1']);

                    //Persist and flush coordinates
                    $this->em->persist($newCoordinates);
                    $this->em->flush();
                }
            }

            //Advance progressbar
            $progressBar->advance();
        }
        //Finish progressbar
        $progressBar->finish();
    }
}
