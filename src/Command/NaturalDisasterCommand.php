<?php

namespace App\Command;

use App\Entity\NaturalDisaster;
use App\Manager\Api\ApiManager;
use App\Repository\CityRepository;
use App\Repository\NaturalDisasterRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NaturalDisasterCommand extends Command
{
    protected static $defaultName = 'app:naturaldisaster:all';

    /**
     * @var NaturalDisasterRepository $naturalDisasterRepository
     */
    private NaturalDisasterRepository $naturalDisasterRepository;

    /**
     * @var CityRepository $cityRepository
     */
    private CityRepository $cityRepository;

    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * @var ApiManager $apiManager
     */
    private ApiManager $apiManager;

    /**
     * NaturalDisasterCommand constructor.
     * @param NaturalDisasterRepository $naturalDisasterRepository
     * @param CityRepository $cityRepository
     * @param EntityManagerInterface $em
     * @param ApiManager $apiManager
     */
    public function __construct(NaturalDisasterRepository $naturalDisasterRepository, CityRepository $cityRepository, EntityManagerInterface $em, ApiManager $apiManager)
    {
        $this->naturalDisasterRepository = $naturalDisasterRepository;
        $this->cityRepository = $cityRepository;
        $this->em = $em;
        $this->apiManager = $apiManager;
        parent::__construct();
    }

    /**
     * NaturalDisasterCommand Configure
     */
    protected function configure()
    {
        $this
            ->setName('app:naturaldisaster:all')
            ->setDescription('Execute app:naturaldisaster to fetch all naturals disasters on API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Get Codes Insee of cities
        $codesInsee = $this->cityRepository->getCodeInseeCities();

        //Launch progressbar
        $progressBar = new ProgressBar($output, count($codesInsee));
        $progressBar->start();

        //Save natural disasters in database
        foreach ($codesInsee as $codeInsee) {

            //Fetch natural disasters in a city via georisques.com
            $data = $this->apiManager->getApiInformation('https://www.georisques.gouv.fr/api/v1/gaspar/catnat?rayon=10&code_insee=' . $codeInsee['codeInsee'] . '&page=1&page_size=100'
            );

            //Check the number of disaster per city
            if (0 !== $data['results']) {

                //Check if disaster is present in database
                foreach ($data['data'] as $naturalDisaster) {

                    // Get city
                    $city = $this->cityRepository->findOneBy(['codeInsee' => $naturalDisaster['code_insee']]);

                    $naturalDisasterExist = $this->naturalDisasterRepository->findBy([
                        'codeNatCatna' => $naturalDisaster['code_national_catnat'],
                        'city' => $city
                    ]);

                    //If the disaster does not exist => Creation
                    if (empty($naturalDisasterExist)) {

                        $newNaturalDisaster = new NaturalDisaster();

                        //Code national CATNAT
                        $newNaturalDisaster->setCodeNatCatna($naturalDisaster['code_national_catnat']);

                        //Event Start Date
                        $eventStartDate = DateTime::createFromFormat('d/m/Y', $naturalDisaster['date_debut_evt']);
                        $eventStartDate->format('Y-m-d');
                        $newNaturalDisaster->setEventStartDate($eventStartDate);

                        //Event End Date
                        $eventEndDate = DateTime::createFromFormat('d/m/Y', $naturalDisaster['date_fin_evt']);
                        $eventEndDate->format('Y-m-d');
                        $newNaturalDisaster->setEventEndDate($eventEndDate);

                        // Publication Order Date
                        $publicationOrderDate = DateTime::createFromFormat('d/m/Y',
                            $naturalDisaster['date_publication_arrete']);
                        $publicationOrderDate->format('Y-m-d');
                        $newNaturalDisaster->setPublicationOrderDate($publicationOrderDate);

                        // Publication JO DATE
                        $publicationJoDate = DateTime::createFromFormat('d/m/Y', $naturalDisaster['date_publication_jo']);
                        $publicationJoDate->format('Y-m-d');
                        $newNaturalDisaster->setPublicationJoDate($publicationJoDate);

                        // Wording Risk JO
                        $newNaturalDisaster->setWordingRiskJo($naturalDisaster['libelle_risque_jo']);

                        //City by code Insee
                        $newNaturalDisaster->setCity($city);

                        //Persist and flush natural disaster
                        $this->em->persist($newNaturalDisaster);
                        $this->em->flush();
                    }
                }
            }
        }

        return command::SUCCESS;
    }
}
