<?php

namespace App\Command;

use App\Entity\City;
use App\Entity\Coordinate;
use App\Entity\Department;
use App\Entity\GeoLocation;
use App\Entity\Regions;
use App\Manager\Api\ApiManager;
use App\Repository\CityRepository;
use App\Repository\DepartmentRepository;
use App\Repository\RegionsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LocationCommand extends Command
{
    protected static $defaultName = 'app.location:all';

    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * @var RegionsRepository $regionsRepository
     */
    private RegionsRepository $regionsRepository;

    /**
     * @var DepartmentRepository $departmentRepository
     */
    private DepartmentRepository $departmentRepository;

    /**
     * @var CityRepository $cityRepository
     */
    private CityRepository $cityRepository;

    /**
     * @var ApiManager $apiManager
     */
    private ApiManager $apiManager;

    /**
     * LocationCommand constructor.
     * @param EntityManagerInterface $em
     * @param RegionsRepository $regionsRepository
     * @param DepartmentRepository $departmentRepository
     * @param CityRepository $cityRepository
     * @param ApiManager $apiManager
     */
    public function __construct(EntityManagerInterface $em, RegionsRepository $regionsRepository, DepartmentRepository $departmentRepository, CityRepository $cityRepository, ApiManager $apiManager)
    {
        $this->em = $em;
        $this->regionsRepository = $regionsRepository;
        $this->departmentRepository = $departmentRepository;
        $this->cityRepository = $cityRepository;
        $this->apiManager = $apiManager;
        parent::__construct();
    }

    /**
     * LocationCommand configuration
     */
    public function configure()
    {
        $this
            ->setName('app:location:all')
            ->setDescription('Execute app:location:all to fetch all locations on API');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        //----------REGIONS-------------
        //Get Region
        $apiResponseRegions = $this->apiManager->getApiInformation('https://geo.api.gouv.fr/regions');

        //Launch progress bar
        $progressBar = new ProgressBar($output, count($apiResponseRegions));
        $progressBar->start();

        //Loop ApiResponseRegions
        foreach ($apiResponseRegions as $region) {

            //Check if exist
            $existRegion = $this->regionsRepository->findOneBy(['code' => $region['code']]);

            // If NOT exist, create new Region
            if (empty($existRegion)) {
                $newRegion = new Regions();

                $newRegion
                    ->setName($region['nom'])
                    ->setCode($region['code']);

                //Persist and flush newRegion
                $this->em->persist($newRegion);
                $this->em->flush();
            }
            //Advance progressbar
            $progressBar->advance();
        }
        //Finish progressbar Region
        $progressBar->finish();

        //----------DEPARTMENTS-------------
        //Get Departments
        $apiResponseDepartments = $this->apiManager->getApiInformation('https://geo.api.gouv.fr/departements');

        //Launch progress bar
        $progressBar = new ProgressBar($output, count($apiResponseDepartments));
        $progressBar->start();

        //Loop ApiResponseDepartments
        foreach ($apiResponseDepartments as $department) {

            //Check if exist
            $existDepartment = $this->departmentRepository->findOneBy(['code' => $department['code']]);

            //If NOT exist, create new Department
            if (empty($existDepartment)) {
                $newDepartement = new Department();

                //Fetch region associated
                $region = $this->regionsRepository->findOneBy(['code' => $department['codeRegion']]);

                //Create new Department
                $newDepartement
                    ->setName($department['nom'])
                    ->setCode($department['code'])
                    ->setRegion($region);

                //Persist and flush
                $this->em->persist($newDepartement);
                $this->em->flush();
            }
            //Advance progressbar
            $progressBar->advance();
        }
        //Finish progressbar Region
        $progressBar->finish();

        //----------CITIES-------------
        //Get Cities
        $apiResponseCities = $this->apiManager->getApiInformation('https://geo.api.gouv.fr/communes');

        //Launch progress bar
        $progressBar = new ProgressBar($output, count($apiResponseCities));
        $progressBar->start();

        //Loop ApiResponseCities
        foreach ($apiResponseCities as $city) {

            //Check if exist
            $existCity = $this->cityRepository->findOneBy(
                ['codeInsee' => $city['code']]);

            //if NOT exist
            if (empty($existCity)) {
                $newCity = new City();

                $department = $this->departmentRepository->findOneBy(['code' => $city['codeDepartement']]);
                $apiResponse = $this->apiManager->getApiInformation('https://geo.api.gouv.fr/communes/' . $city['code'] . '?fields=nom,code,codesPostaux,surface,contour,codeDepartement,codeRegion,population&format=json&geometry=centre');

                //Create new city
                $newCity
                    ->setName($city['nom'])
                    ->setCodeinsee($city['code'])
                    ->setCodePostal($city['codesPostaux']['0'])
                    ->setDepartment($department)
                    ->setPopulate($apiResponse['population'])
                    ->setSurface($apiResponse['surface']);

                //Save GeoLocation City
//                    $newGeoLocation = new GeoLocation();
//
//                    $newGeoLocation->setType($apiResponse['contour']['type']);
//                    $newCity->setGeoLocation($newGeoLocation);
//
//                    //Persist and flush GeoLocation
//                    $this->em->persist($newGeoLocation);
//                    $this->em->flush();
//
//                    //Save gps coordinates
//                    foreach ($apiResponse['contour']['coordinates']['0'] as $detailedCity) {
//
//                        //Checks if the data is of type float
//                        if (is_float($detailedCity['0']) && is_float($detailedCity['1'])) {
//
//                            //Adding data to the database
//                            $newCoordinates = new Coordinate();
//                            $newCoordinates
//                                ->setGpsLon($detailedCity['0'])
//                                ->setGpsLat($detailedCity['1'])
//                                ->setGeoLocation($newGeoLocation);
//
//                            //Persist and flush coordinates
//                            $this->em->persist($newCoordinates);
//                            $this->em->flush();
//                        }
//                    }
                //Persist and flush City
                $this->em->persist($newCity);
                $this->em->flush();

            }
            //Advance progressbar
            $progressBar->advance();
        }
        //Finish progressbar Region
        $progressBar->finish();

        return Command::SUCCESS;
    }
}
