<?php

namespace App\Manager\Api;


use http\Exception\RuntimeException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class ApiManager
 * @package App\Manager
 *
 * Manage all the datas informations between the application and the API
 */
class ApiManager
{

    /**
     * Fetching JSON data via online API
     * @param string $url
     * @return array
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function getApiInformation(string $url): array
    {
        $ApiResponse = $this->apiConnect($url);
        return $ApiResponse->toArray();
    }

    /**
     * Connexion to the API and retrieving JSON informations
     * @param $url
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    private function apiConnect(string $url): ResponseInterface
    {
        $client = HttpClient::create();
        $ApiResponse = $client->request('GET', $url);

        if (200 !== $ApiResponse->getStatusCode()) {
            throw new RuntimeException(sprintf('The API return an error !'));
        }

        return $ApiResponse;
    }
}
