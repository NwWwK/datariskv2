<?php

namespace App\Manager\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;


class UserManager
{
    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * @var UserRepository $userRepository
     */
    private UserRepository $userRepository;


    /**
     * UserManager constructor
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     */
    public function __construct(EntityManagerInterface $em, UserRepository $userRepository)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    public function blockUser(User $user)
    {
        // Check if user is enabled
        $enabled = $user->getIsEnabled();

        //Change the boolean
        ($enabled ? $user->setIsEnabled('0') : $user->setIsEnabled('1'));

        //Persist and flush
        $this->em->persist($user);
        $this->em->flush();
    }

    public function deleteUser(User $user)
    {
        $this->em->remove($user);
        $this->em->flush();
    }

}

