<?php

namespace App\Repository;

use App\Entity\FamilyRisk;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FamilyRisk|null find($id, $lockMode = null, $lockVersion = null)
 * @method FamilyRisk|null findOneBy(array $criteria, array $orderBy = null)
 * @method FamilyRisk[]    findAll()
 * @method FamilyRisk[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilyRiskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FamilyRisk::class);
    }

    // /**
    //  * @return FamilyRisk[] Returns an array of FamilyRisk objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FamilyRisk
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
