<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @param UserSearch $userSearch
     * @return Query
     */
    public function findAllVisibleQuery(UserSearch $userSearch): Query
    {
        $query = $this->findVisibleQuery();

        if ($userSearch->getFirstName()) {
            $query = $query
                ->andWhere('u.firstName LIKE :firstName')
                ->setParameter('firstName', '%' . $userSearch->getFirstName() . '%');
        }


        if ($userSearch->getLastName()) {
            $query = $query
                ->andWhere('u.lastName LIKE :lastname')
                ->setParameter('lastname', '%' . $userSearch->getLastName() . '%');
        }

        if ($userSearch->getEmail()) {
            $query = $query
                ->andWhere('u.email LIKE :email')
                ->setParameter('email', '%' . $userSearch->getEmail() . '%');
        }

        if ($userSearch->isVerified() === true) {
            $query = $query
                ->andWhere('u.isVerified = :isVerified')
                ->setParameter('isVerified', 1);

        } elseif ($userSearch->isVerified() === false) {
            $query = $query
                ->andWhere('u.isVerified = :isVerified')
                ->setParameter('isVerified', 0);
        }

        if ($userSearch->isEnabled() === true) {
            $query = $query
                ->andWhere('u.isEnabled = :isEnabled')
                ->setParameter('isEnabled', false);

        } elseif ($userSearch->isEnabled() === false) {
            $query = $query
                ->andWhere('u.isEnabled = :isEnabled')
                ->setParameter('isEnabled', true);
        }

        if ($userSearch->getRoles()) {
            $query = $query
                ->andWhere('u.roles = :roles')
                ->setParameter('roles', $userSearch->getRoles());
        }

        if ($userSearch->getRegistrationStart()) {
            $query = $query
                ->andWhere('u.registrationDate >= :registrationStart')
                ->setParameter('registrationStart', $userSearch->getRegistrationStart());
        }

        if ($userSearch->getRegistrationEnd()) {
            $query = $query
                ->andWhere('u.registrationDate <= :registrationEnd')
                ->setParameter('registrationEnd', $userSearch->getRegistrationEnd());
        }

        if ($userSearch->getLimit()) {
            $query = $query
                ->setMaxResults($userSearch->getLimit());
        }

        return $query->getQuery();
    }

    public function findVisibleQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->orderBy('u.id', 'DESC');
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
