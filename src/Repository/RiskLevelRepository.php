<?php

namespace App\Repository;

use App\Entity\RiskLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RiskLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method RiskLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method RiskLevel[]    findAll()
 * @method RiskLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RiskLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RiskLevel::class);
    }

    // /**
    //  * @return RiskLevel[] Returns an array of RiskLevel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RiskLevel
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
