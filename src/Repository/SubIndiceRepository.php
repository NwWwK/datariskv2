<?php

namespace App\Repository;

use App\Entity\SubIndice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubIndice|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubIndice|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubIndice[]    findAll()
 * @method SubIndice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubIndiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubIndice::class);
    }

    // /**
    //  * @return SubIndice[] Returns an array of SubIndice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubIndice
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
