<?php

namespace App\Repository;

use App\Entity\NaturalDisaster;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NaturalDisaster|null find($id, $lockMode = null, $lockVersion = null)
 * @method NaturalDisaster|null findOneBy(array $criteria, array $orderBy = null)
 * @method NaturalDisaster[]    findAll()
 * @method NaturalDisaster[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NaturalDisasterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NaturalDisaster::class);
    }

    // /**
    //  * @return NaturalDisaster[] Returns an array of NaturalDisaster objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NaturalDisaster
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
