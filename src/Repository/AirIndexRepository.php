<?php

namespace App\Repository;

use App\Entity\AirIndex;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AirIndex|null find($id, $lockMode = null, $lockVersion = null)
 * @method AirIndex|null findOneBy(array $criteria, array $orderBy = null)
 * @method AirIndex[]    findAll()
 * @method AirIndex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AirIndexRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AirIndex::class);
    }

    // /**
    //  * @return AirIndex[] Returns an array of AirIndex objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AirIndex
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
