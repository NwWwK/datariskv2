<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\City;
use App\Entity\Incident;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class IncidentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nom de l\'incident',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Veuillez renseigner un titre'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un titre'
                    ])
                ]
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'class' => Category::class,
                'choice_label' => 'name',
                'choice_value' => 'name',
                'mapped' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de l\'incident',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Veuillez décrire l\'incident',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez décrire l\'incident'
                    ])
                ]
            ])
            ->add('city', EntityType::class, [
                'label' => 'Ville',
                'class' => City::class,
                'choice_label' => 'name',
                'choice_value' => 'codeInsee',
                'mapped' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('Signaler', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-danger',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Incident::class,
        ]);
    }
}
