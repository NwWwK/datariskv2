<?php

namespace App\Form;

use App\Entity\Action;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ActionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Intitulé : ',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Inscrire l\'intitulé de l\'évenement'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un intitulé',
                    ])
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description : ',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Decription de l\'évenement'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner une description',
                    ])
                ]
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-dark',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Action::class,
        ]);
    }
}
