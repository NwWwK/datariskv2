<?php

namespace App\Form;

use App\Entity\UserSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserSearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nom',
                    'class' => 'form-control-sm'
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Prénom',
                    'class' => 'form-control-sm'
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Email',
                    'class' => 'form-control-sm'
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'required' => false,
                'label' => false,
                'choices' => [
                    'Admin' => '["ROLE_ADMIN"]',
                    'Editeur' => '["ROLE_EDITOR"]',
                    'Utilisateur' => '["ROLE_USER"]'
                ],
            ])
            ->add('isVerified', ChoiceType::class, [
                'required' => false,
                'label' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ]
            ])
            ->add('isEnabled', ChoiceType::class, [
                'required' => false,
                'label' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ]
            ])
            ->add('limit', ChoiceType::class, [
                'required' => false,
                'label' => 'Résultats par page :',
                'attr' => [
                    'class' =>'form-control'
                ],
                'choices' => [
                    '5' => 5,
                    '15' => 15,
                    '30' => 30,
                    '50' => 50,
                    '75' => 75,
                    '100' => 100,
                    '150' => 150
                ],
            ])
            ->add('registrationStart', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date début (jj/mm/aaaa)',
                'required' => false,
                'format' => 'dd/MM/yyyy',
                'html5' => false,

            ])

            ->add('registrationEnd', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date fin (jj/mm/aaaa)',
                'required' => false,
                'placeholder' => ['year' => 'Année', 'month' => 'Mois', 'day' => 'Jour'],
                'format' => 'dd/MM/yyyy',
                'html5' => false,
            ])


            ->add('rechercher', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-dark',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
